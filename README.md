# Supported tags and respective `Dockerfile` links

* [`rc4`, `rc4-2.3`, `latest` (*Dockerfile*)](https://bitbucket.org/hzgwpn/mtangorest.docker/src/c03db0734ac68f15a34706b51f90e2a2e4bb6da0/Dockerfile)

# Usage

provide __TANGO_HOST__ environment variable:

```
docker run -d -e TANGO_HOST=my.host:10000 -p 10001:10001 hzgde/mtangorest.docker:latest 
```

Tango host must have TangoRestServer/sys server defined in the db